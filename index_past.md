
#お願い
##Webアプリ開発に生かすために、インタビューに協力していただける方を探しています!
###<以下のような方からお話を聞きたいと思っています>


####　農学・薬学・理学・歯学・獣医・医学・工学の大学院生の方（４月から大学院に進学する学部４年生の方も含む）

###<アプリのコンセプト>
* Web上の英語の論文やWikipediaなどの記事をスムーズに読めるような機能
* 英語学習（特に単語学習）のサポート

###インタビューしたい内容
* 上記の研究分野で、日頃どのように研究や論文のサーベイが行われているのか
* 研究室のスケジュールなど
* よく参照する科学論文誌（Nature, Science）など

###場所
* 場所を指定していただければ開発者が直接出向きます
* または、情報科学研究科棟のラウンジなどでも可能です

###時間など
* インタビューの時間は30分ほどを予定しています


###連絡先と開発者プロフィール
北海道大学 情報科学研究科 情報理工学専攻 修士課程所属  
名前：石田 祥英（いしだ しょうえい）　炭田 高輝（すみた たかき）<br>
電話：070-5067-8462 （炭田）  
Email：magonote.res.dev@gmail.com   
LineID：takaki12 （炭田）

###理系学生へのアンケートも行っています
ご協力よろしくお願いします！ [アンケートへ](http://goo.gl/forms/t6WyocV56A)  
下のフォームからも回答できます。
<iframe src="https://docs.google.com/forms/d/1YUQdMJpd7XHl1fmaUp3er103GBvrW9Ges3ShaxEqq80/viewform?embedded=true" width="760" height="500" frameborder="0" marginheight="0" marginwidth="0">読み込んでいます...</iframe>



###空いている日程など
募集期間 3月19日（土）から3月28日（火）  
大まかに空いている日程をgoogleカレンダーに掲載しています。  
「予定あり」となっている以外の時間帯で空いているお時間があれば、協力していただけると嬉しいです。
<iframe src="https://calendar.google.com/calendar/embed?src=uhe7rrdjka19a1800jrvabhlvg%40group.calendar.google.com&ctz=Asia/Tokyo" style="border: 0" width="800" height="600" frameborder="0" scrolling="no"></iframe>




#Team Magonoteのプロフィール

北海道大学 情報科学研究科 修士課程所属の２人で作ってます  

WebアプリのPRサイトはこちら  
[http://magonote.tokyo/](http://magonote.tokyo/)





##今後の予定
３月２９日に北海道大学 百年記念館で開催される、北海道大学 産学・地域共同推進機構主催の「北大発ベンチャー促進懇談会」でサービスに関するプレゼンをさせていただきます。詳細は北海道大学産学・地域共同推進機構のホームページでご確認ください。

[北大発ベンチャー促進懇談会 詳細ページ](http://www.mcip.hokudai.ac.jp/cms/cgi-bin/index.pl?page=contents&view_category_lang=1&view_category=1477)



##連絡先
北大情報科学研究科 情報理工学専攻 修士課程所属  
炭田 高輝  
070-5067-8462  
Email : magonote.res.dev@gmail.com   


Webページやロゴのデザイン、UI/UXのプログラミングやサーバーサイドのプログラミング、新規事業立ち上げや新製品のポジショニング戦略などのビジネス面に興味がある方も連絡をお待ちしています!<br>


##北18条門徒歩1分の場所で開発しています
![机](sofa.jpg)
![机](desk.jpg)
