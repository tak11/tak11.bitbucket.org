
#インタビューへのご協力ありがとうございました！
#今後ともよろしくお願いします！

###<アプリのコンセプト>
* Web上の英語の論文やWikipediaなどの記事をスムーズに読めるような機能
* 英語学習（特に単語学習）のサポート


###連絡先と開発者プロフィール
北海道大学 情報科学研究科 情報理工学専攻 修士課程所属  
名前：石田 祥英（いしだ しょうえい）　炭田 高輝（すみた たかき）　　
Email：magonote.res.dev@gmail.com   


###理系学生へのWebアンケートを行っています
ご協力よろしくお願いします！ [アンケートへ](http://goo.gl/forms/t6WyocV56A)  
下のフォームからも回答できます。
<iframe src="https://docs.google.com/forms/d/1YUQdMJpd7XHl1fmaUp3er103GBvrW9Ges3ShaxEqq80/viewform?embedded=true" width="760" height="500" frameborder="0" marginheight="0" marginwidth="0">読み込んでいます...</iframe>

#Team Magonoteのプロフィール

北海道大学 情報科学研究科 修士課程所属の２人で作ってます  

WebアプリのPRサイトはこちら  
[http://magonote.tokyo/](http://magonote.tokyo/)


Webページやロゴのデザイン、UI/UXのプログラミングやサーバーサイドのプログラミング、新規事業立ち上げや新製品のポジショニング戦略などのビジネス面に興味がある方も連絡をお待ちしています!<br>


##北18条門徒歩1分の場所で開発しています
![机](sofa.jpg)
![机](desk.jpg)
